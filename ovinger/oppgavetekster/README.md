# Øvingstekster

Denne mappen inneholder øvingstekster for TDT4100 - Objektorientert programmering våren 2020. Tabellen under viser innleveringsfristene for de forskjellige øvingene, og linker til hver enkelt oppgavetekst.

| Øving             | Innleveringsfrist | Demonstrasjonsfrist |
| ----------------- | ----------------- | ------------------- |
| [Øving 1](oving1) | 22.01.2020        | 29.01.2020          |
| [Øving 2](oving2) | 29.01.2020        | 05.02.2020          |
